﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestingWebApp.Controllers
{
    public class GetSolicitorDataForFundController : ApiController
    {
        // GET: api/GetSolicitorDataForFund
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GetSolicitorDataForFund/5
        public string Get(string sid, string fid)
        {
            return JsonConvert.SerializeObject(API_Library_2._0.Discovery.GetSolicitorDataForFund(sid, fid));
        }

        // POST: api/GetSolicitorDataForFund
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GetSolicitorDataForFund/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GetSolicitorDataForFund/5
        public void Delete(int id)
        {
        }
    }
}
