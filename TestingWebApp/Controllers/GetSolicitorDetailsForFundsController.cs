﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestingWebApp.Controllers
{
    public class GetSolicitorDetailsForFundsController : ApiController
    {
        // GET: api/GetSolicitorDetailsForFunds
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GetSolicitorDetailsForFunds/5
        public string Get(int sid) // this method shouldn't have to be used
        {
            // return JsonConvert.SerializeObject(API_Library_2._0.Discovery.GetSolicitorDetailsForFunds(sid));
            return ("");
        }

        // POST: api/GetSolicitorDetailsForFunds
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GetSolicitorDetailsForFunds/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GetSolicitorDetailsForFunds/5
        public void Delete(int id)
        {
        }
    }
}
