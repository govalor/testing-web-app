﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestingWebApp.Controllers
{
    public class GetLeaderFundDataController : ApiController
    {
        // GET: api/GetLeaderFundData
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GetLeaderFundData/5
        public string Get(int lid, string fid) // shouldn't have to use this method
        {
            //return JsonConvert.SerializeObject(API_Library_2._0.Discovery.GetLeaderFundData(lid, fid));
            return "";
        }

        // POST: api/GetLeaderFundData
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GetLeaderFundData/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GetLeaderFundData/5
        public void Delete(int id)
        {
        }
    }
}
