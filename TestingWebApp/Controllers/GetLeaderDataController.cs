﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestingWebApp.Controllers
{
    public class GetLeaderDataController : ApiController
    {
        // GET: api/GetLeaderData
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GetLeaderData/5
        public string Get(int lid, int fid) // this method shouldn't be used
        {
            //return JsonConvert.SerializeObject(API_Library_2._0.Discovery.GetLeaderData(lid, fid));
            return "";
           
        }

        // POST: api/GetLeaderData
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GetLeaderData/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GetLeaderData/5
        public void Delete(int id)
        {
        }
    }
}
