﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestingWebApp.Controllers
{
    public class GetAdminDetailController : ApiController
    {
        // GET: api/GetAdminDetail
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GetAdminDetail/5
        public string Get(string start, string end)
        {
            return JsonConvert.SerializeObject(API_Library_2._0.Discovery.GetAdminDetail(start, end));
        }

        // POST: api/GetAdminDetail
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GetAdminDetail/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GetAdminDetail/5
        public void Delete(int id)
        {
        }
    }
}
