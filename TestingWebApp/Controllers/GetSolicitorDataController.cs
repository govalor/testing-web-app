﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestingWebApp.Controllers
{
    public class GetSolicitorDataController : ApiController
    {
        // GET: api/GetSolicitorData
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GetSolicitorData/5
        public string Get(int id) // shouldn't have to use this method
        {
            //return JsonConvert.SerializeObject(API_Library_2._0.Discovery.GetSolicitorData(id));
            return "";
        }

        // POST: api/GetSolicitorData
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GetSolicitorData/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GetSolicitorData/5
        public void Delete(int id)
        {
        }
    }
}
