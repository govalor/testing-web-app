﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestingWebApp.Controllers
{
    public class GetSolicitorFundsController : ApiController
    {
        // GET: api/GetSolicitorFunds
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GetSolicitorFunds/5
        public string Get(string nwid)//should eventually be changed to external id
        {
            return JsonConvert.SerializeObject(API_Library_2._0.Discovery.GetSolicitorFunds(nwid));
        }

        // POST: api/GetSolicitorFunds
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GetSolicitorFunds/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GetSolicitorFunds/5
        public void Delete(int id)
        {
        }
    }
}
