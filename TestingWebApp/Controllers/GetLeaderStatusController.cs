﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestingWebApp.Controllers
{
    public class GetLeaderStatusController : ApiController
    {
        // GET: api/GetLeaderStatus
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GetLeaderStatus/5
        public string Get(string lid)
        {
            return JsonConvert.SerializeObject((API_Library_2._0.Discovery.GetLeaderStatus(lid)));
        }

        // POST: api/GetLeaderStatus
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GetLeaderStatus/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GetLeaderStatus/5
        public void Delete(int id)
        {
        }
    }
}
